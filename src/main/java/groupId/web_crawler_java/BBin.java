package groupId.web_crawler_java;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class BBin {
	
	private static int searchIndex;
	
	public static int elementFinder(ArrayList<String> list, String searchNum) {
	    int foundLocation = -1;
	    int i = 0;
	    int j = list.size()-1;
	   
	    // While there are two segments to search, run the block
	    while (i <= j) {
	    	searchIndex = (int) Math.floor((i+j)/2);
	    	
	    	if (searchNum.compareTo(list.get(searchIndex).toString()) > 0) {
	    		i = searchIndex + 1;
	    	} else if (searchNum.compareTo(list.get(searchIndex).toString()) < 0) {
	    		j = searchIndex -1;
	    	} else if (searchNum.compareTo(list.get(searchIndex).toString()) == 0) {
	    		foundLocation = searchIndex;
	    		break;
	    	}
	    }
	    return foundLocation;
	  }
	
	public static void main(String[] args) throws IOException { 
	    	String filePath = "rez.txt";
			ArrayList<String> list = new ArrayList<String>();
			String line;
		    BufferedReader reader = new BufferedReader(new FileReader(filePath));
		    while ((line = reader.readLine()) != null)
		    {
		        list.add(line.split(";")[1]);
		    }
		    reader.close();
		    
		    @SuppressWarnings("resource")
			Scanner sc2 = new Scanner(System.in);
		    System.out.println("PALABRA A BUSCAR:");
		    String arg = sc2.nextLine().toUpperCase();
		    
		    int bs = elementFinder(list, arg);
		    int ll = list.size()-1;
		    
		    if (bs != -1) {
		    	System.out.println("PALABRA ENCONTRADA EN POSICION " + bs);
		    } else if (bs == -1) { 
		    	if (searchIndex == 0) {
		    		System.out.println("NO HAY. Palabras mas cercanas: " + list.get(searchIndex).toString() + ", " + list.get(searchIndex+1).toString());
		    	} else if (searchIndex == ll) {
		    		System.out.println("NO HAY. Palabras mas cercanas: " + list.get(searchIndex-1).toString() + ", " + list.get(searchIndex).toString());
		    	} else if (searchIndex > 0 && searchIndex < ll) {
		    		System.out.println("NO HAY. Palabras mas cercanas: " + list.get(searchIndex-1).toString() + ", " + list.get(searchIndex).toString() + ", " + list.get(searchIndex+1));		    		
		    	}
		    }
    } 

}
