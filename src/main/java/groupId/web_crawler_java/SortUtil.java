package groupId.web_crawler_java;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SortUtil {

	public static void main(String args[]) throws IOException {
		String filePath = "A.txt";
		ArrayList<String> list = new ArrayList<String>();
		String line;
	    BufferedReader reader = new BufferedReader(new FileReader(filePath));
	    while ((line = reader.readLine()) != null)
	    {
	        list.add(line);
	    }
	    reader.close();
	    
	    SortUtil merger = new SortUtil();
		boolean finished = false;
	    @SuppressWarnings("resource")
		Scanner sc2 = new Scanner(System.in);
	    while(!finished)
		{
			System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			System.out.println("~~~~~~~Ordenar y Salir~~~~~~~~");
			System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			System.out.println("1 - Ordenamiento por mezcla CLAVE=CUENTA");
			System.out.println("2 - Ordenamiento por mezcla CLAVE=PALABRA");
			System.out.println("3 - Ordenamiento por seleccion CLAVE=CUENTA");
			System.out.println("4 - Ordenamiento por seleccion CLAVE=PALABRA");
			System.out.println("5 - Salir");
			System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			
			String choice = sc2.nextLine();

			switch(choice) {
			case "1":
				List<String> results1 = merger.merge_sort(list,0);
				System.out.println("Ordenado por mezcla CLAVE=CUENTA. Saliendo...");
				putPrint(results1);
				return;
			case "2":
				List<String> results2 = merger.merge_sort(list,1);
				System.out.println("Ordenado por mezcla CLAVE=PALABRA. Saliendo...");
				putPrint(results2);				
				return;
			case "3":
				List<String> results3 = select(list);
				System.out.println("Ordenado por seleccion CLAVE=CUENTA. Saliendo...");
				putPrint(results3);
				return;
			case "4":
				List<String> results4 = selectS(list);
				System.out.println("Ordenado por seleccion CLAVE=PALABRA. Saliendo...");
				putPrint(results4);
				return;
			case "5":
                return;
			default:
				System.out.println("Debe ingresar un parametro valido. Saliendo...");
				break;
				}	
			}
	    }

	public <T> List<T> merge_sort(List<T> numbers, int mode) {
		if (numbers.size() <= 1) {
			return numbers;
		}
		List<T> right = new ArrayList<T>();
		List<T> left = new ArrayList<T>();

		int middle = numbers.size() / 2;
		for (T num : numbers.subList(0, middle)) {
			left.add(num);
		}
		for (T num : numbers.subList(middle, numbers.size())) {
			right.add(num);
		}
		left = merge_sort(left,mode);
		right = merge_sort(right,mode);

		if (mode == 0) {
			return merge(left, right);
		}else {
			return mergeS(left, right);
		}
	}

	private <T>  List<T> merge(List<T> leftHalf, List<T> rightHalf) {
		List<T> results = new ArrayList<T>();
		while (leftHalf.size() > 0 || rightHalf.size() > 0) {
			if (leftHalf.size() > 0 && rightHalf.size() > 0) {
				boolean diff = false;
				try{
					diff =  Integer.parseInt(leftHalf.get(0).toString().split(";")[0]) >= Integer.parseInt(rightHalf.get(0).toString().split(";")[0]);
				}catch(NumberFormatException e){
					if(leftHalf.get(0).toString().split(";")[0].compareTo(rightHalf.get(0).toString().split(";")[0]) <= 0)
						diff = true;
				}
				if (diff) {
					results.add(leftHalf.get(0));
					if (leftHalf.size() > 1)
						leftHalf = leftHalf.subList(1, leftHalf.size());
					else
						leftHalf = new ArrayList<T>();
				} else {
					results.add(rightHalf.get(0));
					if (rightHalf.size() > 1)
						rightHalf = rightHalf.subList(1, rightHalf.size());
					else
						rightHalf = new ArrayList<T>();
				}
			} else if (leftHalf.size() > 0) {
				results.add(leftHalf.get(0));
				if (leftHalf.size() > 1)
					leftHalf = leftHalf.subList(1, leftHalf.size());
				else
					leftHalf = new ArrayList<T>();
			} else if (rightHalf.size() > 0) {
				results.add(rightHalf.get(0));
				if (rightHalf.size() > 1)
					rightHalf = rightHalf.subList(1, rightHalf.size());
				else
					rightHalf = new ArrayList<T>();
			}
		}
		return results;
	}
	
	private <T>  List<T> mergeS(List<T> leftHalf, List<T> rightHalf) {
		List<T> results = new ArrayList<T>();
		while (leftHalf.size() > 0 || rightHalf.size() > 0) {
			if (leftHalf.size() > 0 && rightHalf.size() > 0) {
				boolean diff = false;
				try{
					diff =  Integer.parseInt(leftHalf.get(0).toString().split(";")[1]) <= Integer.parseInt(rightHalf.get(0).toString().split(";")[1]);
				}catch(NumberFormatException e){
					if(leftHalf.get(0).toString().split(";")[1].compareTo(rightHalf.get(0).toString().split(";")[1]) <= 0)
						diff = true;
				}
				if (diff) {
					results.add(leftHalf.get(0));
					if (leftHalf.size() > 1)
						leftHalf = leftHalf.subList(1, leftHalf.size());
					else
						leftHalf = new ArrayList<T>();
				} else {
					results.add(rightHalf.get(0));
					if (rightHalf.size() > 1)
						rightHalf = rightHalf.subList(1, rightHalf.size());
					else
						rightHalf = new ArrayList<T>();
				}
			} else if (leftHalf.size() > 0) {
				results.add(leftHalf.get(0));
				if (leftHalf.size() > 1)
					leftHalf = leftHalf.subList(1, leftHalf.size());
				else
					leftHalf = new ArrayList<T>();
			} else if (rightHalf.size() > 0) {
				results.add(rightHalf.get(0));
				if (rightHalf.size() > 1)
					rightHalf = rightHalf.subList(1, rightHalf.size());
				else
					rightHalf = new ArrayList<T>();
			}
		}
		return results;
	}
	
	public static ArrayList<String> select(ArrayList<String> input) {
		String temp;
		for (int i = 0; i < input.size(); i++) {
			for (int j = i; j > 0; j--) {
				if (Integer.parseInt(input.get(j).toString().split(";")[0]) > Integer.parseInt(input.get(j-1).toString().split(";")[0])) {
					temp = input.get(j);
					input.set(j, input.get(j-1));
					input.set(j-1, temp);
				}
			}
		}
		return input;
	}
	
	public static ArrayList<String> selectS(ArrayList<String> input) {
		String temp;
		for (int i = 1; i < input.size(); i++) {
			for (int j = i; j > 0; j--) {
				if (input.get(j).toString().split(";")[1].compareToIgnoreCase(input.get(j-1).toString().split(";")[1]) < 0) {
					temp = input.get(j);
					input.set(j, input.get(j-1));
					input.set(j-1, temp);
				}
			}
		}
		return input;
	}
	
	public static void putPrint(List<String> results) {
		try {
	    	PrintStream o = new PrintStream(new File("rez.txt"));
		    System.setOut(o);
	    	for (String i : results) {
	    		System.out.println(i);
	    		}
	    	}catch (FileNotFoundException e) {
				e.printStackTrace();
	    	}
	}
}