package groupId.web_crawler_java;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;

public class WebCrawler{
    // Constructor
    public WebCrawler() {
        links = new HashSet<String>();
    }

    // Control
    private static int MAX_DEPTH = 1;
    
    // TADS
    private HashSet<String> links;
    static ArrayList<String> words;
    static ArrayList<String> wordzie = new ArrayList<String>();
    static HashMap<String, Integer> mapa = new HashMap<String, Integer>();
    
    public void getPageLinks(String URL, int depth) {
        // Ensure non-prior visit and max-depth not reached
    	String htmlWords= null;
        if (!(links.contains(URL)) && (depth < MAX_DEPTH)) {
            System.out.println("Nivel: " + depth + " [" + URL + "]");
            try {
                // Append URL
                links.add(URL);

                // HTML a descuartizar
                Document document = Jsoup.connect(URL).get();
                Elements linksOnPage = document.select("a[href]");
                htmlWords = Jsoup.parse(document.html()).text().toUpperCase();
                getPageWords(htmlWords);
                htmlWords = null;
                depth++;

                for (Element page : linksOnPage) {
                    getPageLinks(page.attr("abs:href"), depth);
                }
            } catch (IOException e) {
                System.err.println("For '" + URL + "': " + e.getMessage());
            }
        }
    }
    public void getPageWords(String a) { 
    	//words = new ArrayList<String>(Arrays.asList(a.split("[^a-zA-ZZáéíñóúüÁÉÍÑÓÚÜ']+")));
    	words = new ArrayList<String>(Arrays.asList(a.split("[^A-ZÁÉÍÑÓÚÜ']+")));
    	//System.out.println(Arrays.toString(words.toArray()));
    	wordzie.addAll(words);
    }
    
    public static void toHashM() throws FileNotFoundException {
    	for (String str : wordzie) {
    	    if (mapa.containsKey(str)) {
    	        mapa.put(str, mapa.get(str) + 1);
    	    } else {
    	        mapa.put(str, 1);
    	    }
    	}
    	PrintStream o = new PrintStream(new File("A.txt"));
    	System.setOut(o); 
    	for (Map.Entry<String, Integer> entry : mapa.entrySet()) {
    	    System.out.println(entry.getValue() + ";" + entry.getKey());
    	}
    }

    public static void main(String[] args) throws FileNotFoundException {
	    System.out.println("SITIO:");
    	@SuppressWarnings("resource")
		Scanner sc1 = new Scanner(System.in);
	    String arg = sc1.nextLine();
	    System.out.println("Nivel:");
	    @SuppressWarnings("resource")
		Scanner sc2 = new Scanner(System.in);
	    String dh = sc2.nextLine();
    	MAX_DEPTH = Integer.parseInt(dh);
    	//new WebCrawler().getPageLinks("https://www.twitter.com/", 0);
	    new WebCrawler().getPageLinks(arg, 0);
    	toHashM();
       }
}
