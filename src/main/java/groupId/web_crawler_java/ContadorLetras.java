package groupId.web_crawler_java;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ContadorLetras {
	
	private static DecimalFormat df2 = new DecimalFormat("#.##");
	static void characterCount(ArrayList<String> list) 
    { 
        // Creating a HashMap containing char 
        // as a key and occurrences as  a value 
        HashMap<Character, Integer> charCountMap = new HashMap<Character, Integer>(); 
        int k = 0;
  
        // checking each char 
        for (char c : list.toString().toCharArray()) { 
            if (charCountMap.containsKey(c) && c != ',' && c != '[' && c != ']' && c != ' ') { 
                // If char is present in charCountMap, 
                // incrementing it's count by 1 
                charCountMap.put(c, charCountMap.get(c) + 1);
                k++;
            } 
            else { 
                // If char is not present in charCountMap, 
                // putting this char to charCountMap with 1 as it's value 
                charCountMap.put(c, 1); 
                k++;
            } 
        } 
  
        // Printing the charCountMap 
        for (@SuppressWarnings("rawtypes") Map.Entry entry : charCountMap.entrySet()) {
        	String value = entry.getValue().toString();
        	int intValue = Integer.parseInt(value)*100;
        	double d = (double) intValue/k;
        	System.out.println(entry.getKey() + " " + value + " -> " + df2.format(d) + "%");
        } 
    } 
  
    // Driver Code 
    public static void main(String[] args) throws IOException 
    { 
    	String filePath = "rez.txt";
		ArrayList<String> list = new ArrayList<String>();
		String line;
	    BufferedReader reader = new BufferedReader(new FileReader(filePath));
	    while ((line = reader.readLine()) != null)
	    {
	        list.add(line.split(";")[1]);
	    }
	    reader.close();
	    
	    @SuppressWarnings("resource")
		BufferedReader Buff = new BufferedReader(new FileReader(filePath));
        String text = Buff.readLine();
        System.out.println("Palabra mas repetida: " + text);
        characterCount(list); 
    } 

}
